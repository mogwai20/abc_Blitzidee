const { app, BrowserWindow, ipcMain } = require('electron/main')
const path = require('node:path')
const tableMode = true;

const createWindow = () => {
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js')
    } 
  })

  win.loadFile('./pages/rightWrong.html')

  /* if (tableMode==true) {
    
  }else{
    win.loadFile('./pages/start.html')
  } */
  
  }

app.whenReady().then(() => {
    //damit kann man massages vom renderer zum main process sen den durch den ping channel
  ipcMain.handle('ping', () => 'pong')
  createWindow()

  //falls kein Window offen, create ein neues (für MAC, da die da gerne im Hitergund laufen))
  app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })
})
 
//wenn alle Fenster geschlossen soll die App beendet werden (wichtig für MAC)
app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') app.quit()
  })